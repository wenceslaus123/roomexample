package com.cohero.roomexample;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LibDataBase db = Room.databaseBuilder(getApplicationContext(),
                LibDataBase.class, "populus-database").build();

        new Thread(new Runnable() {
            @Override
            public void run() {
                db.getBookDao().deleteAll();
                for (int i = 0; i < 100; i++) {
                    db.getBookDao().insertAll(
                            new Book(i, "Vasya" + i, "Summer" + i, 1900 + i)
                    );
                }

                List<Book> books = db.getBookDao().getAllBookWithYear(1990);
                for (Book book : books) {
                    Log.d("book", book.toString());
                }
            }
        }).start();
    }
}
