package com.cohero.roomexample;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Honchar.V on 10-Apr-18.
 */
@Database(entities = {Book.class /*, AnotherEntityType.class, AThirdEntityType.class */}, version = 1)
public abstract class LibDataBase extends RoomDatabase {
    public abstract BookDao getBookDao();
}
