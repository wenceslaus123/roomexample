package com.cohero.roomexample;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by Honchar.V on 10-Apr-18.
 */
@Dao
public interface BookDao {

    @Insert
    void insertAll(Book... people);

    @Delete
    void delete(Book person);

    @Query("SELECT * FROM book")
    List<Book> getAllBook();

    @Query("SELECT * FROM book WHERE year < :year")
    List<Book> getAllBookWithYear(int year);

    @Query("DELETE FROM book")
    void deleteAll();
}
